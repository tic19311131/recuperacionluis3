import React, { useEffect } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import Feed from './components/Feed';
import { Navigation } from 'react-native-navigation';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Product from './screens/Product'
import Test from './screens/Test';
import Home from './screens/Home';




const Stack = createStackNavigator();
export default function App() {
  
  
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="product" component = {Product} />
          <Stack.Screen name="Test" component={Test} />

        </Stack.Navigator>
    </NavigationContainer>
        
    ); 
}
