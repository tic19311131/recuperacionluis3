import React, { useState, useEffect } from "react";
import { StyleSheet, ScrollView, View, Text, Button, SafeAreaView} from 'react-native';
import axios from "axios";
import Post from './Post';


function Feed( navigation ) {

     


    const [data, setData] = useState([]);
    useEffect( () => {
      (async ()=>{
        try{
          const result = await axios.get(`${window.location.protocol}//${window.location.hostname}:8000/api/product/`);
          setData(()=> result.data);

        }
        catch(error) {
          console.log(error);
        }
      })()
    }, []);

    
    return (
        <SafeAreaView style={styles.container} >
          
        {
        //     Object.entries(data).map(item => (   
        //         // console.log(item[1].name)
        //     )
        
        data.length>0?data.map((post) =>( 
        <View  style={styles.separator}> 
            <Post navigation={navigation.navigation} style={styles.separator}
            key={post.id}
            name={post.name}
            dato={post.id}
            />
            </View>
          )):null
         
        }
        
        </SafeAreaView>
    )

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    height: 50,
    width: '80%',
    borderWidth:1,
    borderColor:"Black",
    margin:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Feed;