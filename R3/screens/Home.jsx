import React, { useEffect } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Feed from '../components/Feed.jsx';

export default function Home( { navigation }) {

    return (
          <View   >
            <Feed navigation={ navigation }/>
            <View>
            </View>
        </View>
        
    ); 
}