import React, { useState, useEffect } from "react";
import { StyleSheet, ScrollView,View,Text, SafeAreaView, Image , Container} from 'react-native';
import axios from "axios";
import Post from '../components/Post';







function ProductById( { route, navigation }) {
    
    
    const {id} = route.params;
    console.log(id);
    const [data, setData] = useState([]);
    useEffect( () => {
      (async ()=>{
        try{
          const result = await axios.get(`${window.location.protocol}//${window.location.hostname}:8000/api/product/`+id);
          setData(()=> result.data);
        }
        catch(error) {
          console.log(error);
        }
      })()
    }, []);

// console.log(data);

    return (
       
      <SafeAreaView style={styles.all} >
        <View style={styles.container}>

          <View style={styles.image} >
            <Image style={{ width: 150, height: 150}} 
            source={{uri:" https://i.blogs.es/eb7740/iphone-11-pro/450_1000.jpg"}} />
          </View>

          <View style={styles.text}>
            <Text style={styles.title} > {data.name}</Text>
            <Text style={styles.separator}>Description: {data.description}</Text>
            <Text style={styles.separator}>Brand: {data.brand}</Text>
            <Text style={styles.separator}>Category: {data.category}</Text>
            <Text style={styles.separator}>$ {data.price}</Text>
         </View>

        </View>
      </SafeAreaView>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "lightgreen",
    margin:50,
    marginTop:20,
    
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    height: 50,
    width: '80%',
    borderWidth:1,
    borderColor:"Black",
    alignItems: 'center',
    justifyContent: 'center',
  },
  image:{
    borderWidth:1,
    border:"danger",
    marginTop:10,
  },
  all:{
    flex:2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    justifyContent: 'center',
    alignItems: 'center',
   
  }
});

export default ProductById;