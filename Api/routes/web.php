<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use Illuminate\Contracts\Session\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

//  Route::post('/logout', function () {

//      Session::forget('user');
//     return redirect ('login');
//  });

//UserController
Route::post('/login',[UserController::class,'login']);

//ProductController
Route::get('/product',[ProductController::class,'getProduct']);
Route::get('/detail/{id}',[ ProductController::class,'details']);
Route::get('/home',[ProductController::class,'index']);
Route::post('/kart',[ProductController::class,'addToKart']);


//kartController

