<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


//Rutas de producto
 Route::get('product','App\Http\Controllers\productcontroller@getProduct');
 Route::get('product/{id}','App\Http\Controllers\productcontroller@getProductId');
 Route::post('addProduct','App\Http\Controllers\productcontroller@insertProduct');
 Route::put('updateProduct/{id}','App\Http\Controllers\productcontroller@updateProduct');
 Route::delete('deleteProduct/{id}','App\Http\Controllers\productcontroller@deleteProduct');
    
