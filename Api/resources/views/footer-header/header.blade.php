<?php
use App\Http\Controllers\ProductController;
$total=0;
if (Session::has('user')) {
    $total=ProductController::kartItem();
}
?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/home" image="../images/">Servicell</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          {{-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
          <li><a href="#">Link</a></li> --}}
          {{-- Products list --}}
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Glasess</a></li>
              <li><a href="#">Cases</a></li>
              <li><a href="#">Chargers</a></li>
              <li><a href="#">Cellphones</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">All products</a></li>
            </ul>
          </li>
          {{-- Brands List --}}
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Brands <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Samsung</a></li>
              <li><a href="#">Motorola</a></li>
              <li><a href="#">iPhone</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">All Brands</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-form navbar-left">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#">Kart({{ $total }})</a></li>
          {{-- User settings --}}
          <li class="dropdown">
              @if (Session::has('user') )
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Session::get('user')["name"] }}<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">My account</a></li>
              <li><a href="#">Orders</a></li>
              <li><a href="#">Wish List</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="/logout">Logout</a></li>
            </ul>
            </li>
        @else
        @endif
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
