@extends('index')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <img class="detail-img" src="{{$product['image']}}">
        </div>

        <div class="col-sm-6">
            <a href="/home">Go back</a>
            <h2>{{ $product['name'] }}</h2>
            <h4>Details: </h4>
            <h4>{{ $product['description'] }}</h4>
            <h4>Brand: {{ $product['brand'] }}</h4>
            <h4>Category: {{ $product['category'] }}</h4>
            <h3>Price: $  </h3>
            <h3>{{ $product['price'] }}</h3>
            <br>
            <br>
            <form method="POST" action="/kart">
                @csrf
                <input type="hidden" name="product_id" value="{{ $product['id'] }}">
            <button class="btn btn-primary" >Add to Cart</button>
            </form>
            <button class="btn btn-success">Buy Now</button>
        </div>

    </div>

</div>
@endsection
