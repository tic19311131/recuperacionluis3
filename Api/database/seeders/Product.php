<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\support\facades\DB;


class Product extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name'=>'iPhone',
                'description'=>'iPhone 7 Plus GoldRose 64 Gb',
                'brand'=>'iPhone',
                'price'=>'8500',
                'category'=>'SmartPhone',
                'image'=>'https://bsl.cwa.sellercloud.com/images/products/305024.jpg'
            ],
            [
                'name'=>'iPhone',
                'description'=>'iPhone 7 Plus GoldRose 64 Gb',
                'brand'=>'iPhone',
                'price'=>'8500',
                'category'=>'SmartPhone',
                'image'=>'https://bsl.cwa.sellercloud.com/images/products/305024.jpg'
            ],
            [
                'name'=>'iPhone',
                'description'=>'iPhone 7 Plus GoldRose 64 Gb',
                'brand'=>'iPhone',
                'price'=>'8500',
                'category'=>'SmartPhone',
                'image'=>'https://bsl.cwa.sellercloud.com/images/products/305024.jpg'
            ],
            [
                'name'=>'iPhone',
                'description'=>'iPhone 7 Plus GoldRose 64 Gb',
                'brand'=>'iPhone',
                'price'=>'8500',
                'category'=>'SmartPhone',
                'image'=>'https://bsl.cwa.sellercloud.com/images/products/305024.jpg'
            ]
        ]);

    }
}
