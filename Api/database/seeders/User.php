<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\support\facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'=>'Juan',
                'lastName'=>'Apodaca',
                'secondLn'=>'',
                'phone'=>'6681034695',
                'email'=>'apodaca1@gmail.com',
                'password'=>Hash::make('123456') 
            ],
            [
                'name'=>'ykjrty',
                'lastName'=>'csaf',
                'secondLn'=>'',
                'phone'=>'6681034665',
                'email'=>'3@gmail.com',
                'password'=>Hash::make('123456') 
            ],
            [
                'name'=>'nrfhg',
                'lastName'=>'sdfsdf',
                'secondLn'=>'',
                'phone'=>'4681034695',
                'email'=>'2@gmail.com',
                'password'=>Hash::make('123456') 
            ]
        ]);

    }
}
