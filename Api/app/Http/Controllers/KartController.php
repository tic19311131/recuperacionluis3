<?php

namespace App\Http\Controllers;

use App\Models\Kart;
use App\Models\ProductController;
use Illuminate\Http\Request;

class KartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kart  $kart
     * @return \Illuminate\Http\Response
     */
    public function show(Kart $kart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kart  $kart
     * @return \Illuminate\Http\Response
     */
    public function edit(Kart $kart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kart  $kart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kart $kart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kart  $kart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kart $kart)
    {
        //
    }

    // function addToKart(Request $request){

    //     if($request->session()->has('user'))
    //     {
    //         $kart=new Kart;
    //         $kart->user_id=$request->session()->get('user')['id'];
    //         $kart->product_id=$request->product_id;
    //         $kart->save();
    //         return redirect('/home');


    //     }else{
    //         redirect('/login');
    //     }

    // }
}
