<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;


class UserController extends Controller
{
    function login(Request $request){

        $user = User::where(['email' => $request->email])->first();
        if( !$user or !Hash::check($request->password, $user->password))
        {
            return "Username or Password is not matched";
        }else{
            $request->session()->put('user',$user);
            return redirect('/product');
        }
    }

    // public function logout(Request $request) {
    //     Auth::logout();
    //     return redirect('/login');
    //   }

}
