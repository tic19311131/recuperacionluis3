<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product;
use App\Models\Kart;
use Session;

class ProductController extends Controller
{
   function index(){
       $data=Product::all();
       return view('/home',['products'=>$data]);
   }

   function details($id){
       $data= Product::find($id);
       return view('product/details',['product'=>$data]);

   }

    function addToKart(Request $request){
        if($request->session()->has('user'))
        {
            $kart=new Kart;
            $kart->user_id=$request->session()->get('user')['id'];
            $kart->product_id=$request->product_id;
            $kart->save();
            return redirect('/home');


        }else{
            redirect('/login');
        }

    }

    static function kartItem()
    {
        $userId=Session::get('user')['id'];
        return Kart::where('user_id',$userId)->count();
    }



    public function getProduct(){
        return response()->json(product::all(),200);
        $products = Product::all();
        return view('product/products',compact('products'));
    }

    public function getProductId($id){
        $product = product::find($id);
        if(is_null($product)){
            //404 error de no encontrado
            return response()->json(['Message'=>'Product not found'],404);

        }
        return response()->json($product::find($id), 200);
    }

    public function insertProduct(Request $request){
        $product = product::create($request->all());
        return response($product,200);
    }

    public function updateProduct(request $request, $id){
        $product=product::find($id);
        if(is_null($product)){
            //404 error de no encontrado
            return response()->json(['Message'=>'Product not found'],404);
        }
        $product->update($request->all());
        return response ($product, 200);
    }

    public function deleteProduct($id){
        $product = product::find($id);
        if(is_null($product)){
            return response()->json(['Message'=>'Product not found'],404);
        }
        $product->delete();
        return response()->json(['Message'=>'Product deleted'],200);
    }

}

